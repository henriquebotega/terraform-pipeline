variable "location" {
  type    = string
  default = "Canada Central"
}

variable "aws_pub_key" {
  type    = string
}

variable "azure_pub_key" {
  type    = string
}
