terraform {
  required_version = ">= 1.0.0"

  required_providers {
    azurerm = {
      version = "2.94.0"
      source  = "hashicorp/azurerm"
    }
  }

  backend "azurerm" {
    resource_group_name  = "remote-state"
    storage_account_name = "abotegaremotestate"
    container_name       = "remote-state"
    key                  = "azure-vm-pipeline/terraform.tfstate"
  }
}

provider "azurerm" {
  features {}
}

data "terraform_remote_state" "vnet" {
  backend = "azurerm"

  config = {
    resource_group_name  = "remote-state"
    storage_account_name = "abotegaremotestate"
    container_name       = "remote-state"
    key                  = "azure-vnet/terraform.tfstate"
  }
}

provider "aws" {
  region = "ca-central-1"

  default_tags {
    tags = {
      owner      = "abotega"
      managed-by = "terraform"
    }
  }
}

data "terraform_remote_state" "vpc_remote" {
  backend = "s3"

  config = {
    bucket = "abotega-remote-state"
    key    = "aws-vpc/terraform.tfstate"
    region = "ca-central-1"
  }
}