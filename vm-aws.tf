resource "aws_key_pair" "key" {
  key_name   = "key"
  # public_key = file("./aws-key.pub")
  public_key = file(var.aws_pub_key)
}

resource "aws_instance" "vm" {
  ami           = "ami-0b6937ac543fe96d7"
  instance_type = "t2.micro"

  key_name                    = aws_key_pair.key.key_name
  subnet_id                   = data.terraform_remote_state.vpc_remote.outputs.subnet_id
  vpc_security_group_ids      = [data.terraform_remote_state.vpc_remote.outputs.security_group_id]
  associate_public_ip_address = true

  tags = {
    "Name" = "vm-terraform"
  }
}